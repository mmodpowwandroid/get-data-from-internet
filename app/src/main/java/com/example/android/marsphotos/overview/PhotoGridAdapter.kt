package com.example.android.marsphotos.overview
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.android.marsphotos.databinding.GridViewItemBinding
import com.example.android.marsphotos.network.MarPhoto

class PhotoGridAdapter : ListAdapter<MarPhoto, PhotoGridAdapter.MarPhotoViewHolder>(DiffCallback) {
    class MarPhotoViewHolder(private var binding:GridViewItemBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(MarsPhoto: MarPhoto) {
            binding.photo = MarsPhoto
            binding.executePendingBindings()
        }
    }
    fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoGridAdapter.MarPhotoViewHolder {
        return MarPhotoViewHolder(GridViewItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    fun onBindViewHolder(holder: PhotoGridAdapter.MarPhotoViewHolder, position: Int) {
        val marsPhoto = getItem(position)
        holder.bind(marsPhoto)
    }

    companion object DiffCallback: DiffUtil.ItemCallback<MarPhoto>() {
        override fun areItemsTheSame(oldItem: MarPhoto, newItem: MarPhoto): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: MarPhoto, newItem: MarPhoto): Boolean {
            return oldItem.imgSrcUrl == newItem.imgSrcUrl
    }

}
    }